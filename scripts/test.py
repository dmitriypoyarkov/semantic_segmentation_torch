import cv2 as cv

import semantic_segmentation_torch.package_settings as settings
from semantic_segmentation_torch.torch_seg_plugin import TorchSegPlugin

if __name__ == '__main__':
    settings.initialize('semantic_segmentation_torch')
    plugin = TorchSegPlugin()

    img = cv.imread(settings.data_path('test.png'))
    res = plugin.segment_img(img)

    cv.imshow('fff.png', res)
    cv.waitKey(0)
    cv.destroyAllWindows()
