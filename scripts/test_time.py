import time

import cv2 as cv

import semantic_segmentation_torch.package_settings as settings
from semantic_segmentation_torch.torch_seg_plugin import TorchSegPlugin

if __name__ == '__main__':
    settings.initialize('semantic_segmentation_torch')
    plugin = TorchSegPlugin()

    img = cv.imread(settings.data_path('test.png'))

    n_tries = 10
    time_measurements = []
    for i in range(n_tries):
        start = time.time()
        res = plugin.segment_img(img)
        time_measurement = time.time() - start
        time_measurements.append(time_measurement)
        print(f'Try {i}: time spent {time_measurement:.03f}')

    total_time = sum(time_measurements)
    print(f'Total time: {total_time:.03f}\nAverage time: {(total_time/n_tries):.03f}')
