import warnings

from semantic_segmentation_torch.configs import MyConfig, load_parser
from semantic_segmentation_torch.core import SegTrainer

warnings.filterwarnings("ignore")


if __name__ == '__main__':
    config = MyConfig()

    config.init_dependent_config()

    # If you want to use command-line arguments, please uncomment the following line
    # config = load_parser(config)

    trainer = SegTrainer(config)

    if config.is_testing:
        trainer.predict(config)
    else:
        trainer.run(config)