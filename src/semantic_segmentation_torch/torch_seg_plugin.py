import warnings
from typing import List

import numpy as np
# from cross_view_localization.plugins.cvl_seg_plugin import CVLSegPlugin
# from cross_view_localization.tools.warp_data import WarpDataSet
from cv2.typing import MatLike

from semantic_segmentation_torch.configs import MyConfig
from semantic_segmentation_torch.core import SegTrainer

warnings.filterwarnings("ignore")


# class TorchSegPlugin("CVLSegPlugin"):
class TorchSegPlugin(object):
    def __init__(self, label_colors = None):
        self._config = MyConfig()
        self._config.init_dependent_config()
        self._trainer = SegTrainer(self._config)
        self._label_colors = label_colors

    @property
    def label_colors(self):
        return self._label_colors

    @label_colors.setter
    def label_colors(self, value : np.ndarray):
        self._label_colors = value


    def ready(self):
        return True


    def get_segmented(self, warp_dataset):
        seg_imgs = []
        for warp_data in warp_dataset.warp_datas:
            seg_labels = self._trainer.predict_with_labels(warp_data.rgb_img)
            seg_sys_colors = self.apply_palette(seg_labels)
            seg_imgs.append(seg_sys_colors)
        return seg_imgs


    def apply_palette(self, label_img : MatLike):
        ''' Convert label_img (matrix with label numbers (like 0, 1, 2, 3)) to matrix with colors. '''
        if self._label_colors is None:
            raise RuntimeError('label_colors is None.' +
                'They are required for the apply_palette() function'
            )
        colorful_img : MatLike = np.zeros(
            (label_img.shape[0], label_img.shape[1], 3),
            dtype=np.uint8
        )
        for label_index in range(len(self._label_colors)):
            colorful_img[label_img == label_index] = np.array(self._label_colors[label_index])
        return colorful_img


    def segment_img(self, img : MatLike):
        if self._label_colors is None:
            return self._trainer.predict_with_colormap(img)

        seg_labels = self._trainer.predict_with_labels(img)
        seg_sys_colors = self.apply_palette(seg_labels)
        return seg_sys_colors