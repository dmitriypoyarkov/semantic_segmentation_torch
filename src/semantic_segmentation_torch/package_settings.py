import os
from typing import Callable

import rospkg


class PackagePath(object):
    '''Class for retreiving absolute paths to package folders.'''
    def __init__(self, package_name : str):
        '''
        Parameters:
        - package_name - Path to package
        '''
        rospack = rospkg.RosPack()
        self._package_path = rospack.get_path(package_name)


    def abs_path(self, rel_path : str) -> str:
        return os.path.join(
            self._package_path, rel_path
        )

initialized = False

def initialize(package_name):
    global pack, initialized
    pack = PackagePath(package_name)
    initialized = True


def require_initialized(fun : Callable):
    def wrapper(*args):
        if not initialized:
            raise RuntimeError("Package settings not initialized")
        return fun(*args)
    return wrapper

@require_initialized
def abs_path(path : str):
    return pack.abs_path(path)

@require_initialized
def save_path(path : str):
    return pack.abs_path(os.path.join('save/', path))

@require_initialized
def data_path(path : str):
    return pack.abs_path(os.path.join('data/', path))

@require_initialized
def checkpoints_path(path : str):
    return pack.abs_path(os.path.join('checkpoints/', path))