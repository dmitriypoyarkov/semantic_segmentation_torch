#!/usr/bin/env python3

import json
import threading
import time
from typing import List

import numpy as np
import rospy
from cv_bridge import CvBridge
from sensor_msgs.msg import Image
from std_msgs.msg import String

from semantic_segmentation_torch.torch_seg_plugin import TorchSegPlugin


class ImgSegChannel(object):
    BRIDGE = CvBridge()

    def __init__(
            self, input_topic, out_topic,
            seg_plugin : TorchSegPlugin,
            run_without_label_colors = True
        ):
        self._input_topic = input_topic
        self._out_topic = out_topic
        self._sub = rospy.Subscriber(
            input_topic, Image, self.img_callback, queue_size=1,
        )
        self._pub = rospy.Publisher(out_topic, Image, queue_size=1)
        self._seg_plugin = seg_plugin
        self._label_colors = None
        self._run_without_label_colors = run_without_label_colors

        self._last_imgmsg = None

    @property
    def input_topic(self):
        return self._input_topic

    @property
    def out_topic(self):
        return self._out_topic

    @property
    def label_colors(self):
        raise ValueError('Property is set-only')

    @label_colors.setter
    def label_colors(self, value : np.ndarray):
        self._label_colors = value


    def img_callback(self, msg : Image):
        self._last_imgmsg = msg


    def seg_and_pub_last_msg(self):
        if self._last_imgmsg is None:
            return
        self.seg_and_pub(self._last_imgmsg)
        self._last_imgmsg = None


    def seg_and_pub(self, msg : Image):
        if not self._run_without_label_colors and self._seg_plugin.label_colors is None:
            return
        img_cv = self.BRIDGE.imgmsg_to_cv2(msg, 'passthrough')
        seg = self._seg_plugin.segment_img(img_cv)
        seg_msg = self.BRIDGE.cv2_to_imgmsg(seg, 'rgb8')
        seg_msg.header = msg.header
        self._pub.publish(seg_msg)


class ImgSegNode(object):
    def __init__(self, max_channels=10):
        rospy.init_node('seg_node')
        self._seg_plugin = TorchSegPlugin()
        self.config_label_colors()

        self._seg_channels : List[ImgSegChannel] = []
        for i in range(max_channels):
            next_topic = rospy.get_param(
                f'~input_img_topic_{i}', default=''
            )
            if next_topic == '':
                continue
            out_topic = rospy.get_param(
                f'~out_img_topic_{i}'
            )
            self._seg_channels.append(
                ImgSegChannel(
                    next_topic, out_topic,
                    seg_plugin=self._seg_plugin,
                    run_without_label_colors=self._run_without_label_colors
                )
            )
        rospy.loginfo(f'INPUT TOPICS: {list(map(lambda ch : ch.input_topic, self._seg_channels))}')
        rospy.loginfo(f'OUT TOPICS: {list(map(lambda ch : ch.out_topic, self._seg_channels))}')
        pub_thread = threading.Thread(target=self.publisher)
        pub_thread.start()


    def config_label_colors(self):
        self._run_without_label_colors = bool(
            rospy.get_param('~run_without_label_colors')
        )
        self._label_colors_topic = str(
            rospy.get_param('~input_label_colors', default='')
        )
        if (not self._run_without_label_colors and
                self._label_colors_topic == ''):
            raise RuntimeError(
                'SEG_NODE: input_label_colors not specified, but ' +
                'run_without_label_colors is True. This is not allowed'
            )
        if len(self._label_colors_topic) > 0:
            self._label_colors_sub = rospy.Subscriber(
                self._label_colors_topic, String,
                self.label_colors_callback
            )
        if self._run_without_label_colors:
            rospy.loginfo(f'SEG_NODE: Running without label_colors for now.')
        else:
            rospy.loginfo(f'SEG_NODE: Waiting for topic {self._label_colors_topic}...')


    def label_colors_callback(self, msg : String):
        colors = np.array(json.loads(msg.data))
        self._seg_plugin.label_colors = colors
        self._label_colors_sub.unregister()
        rospy.loginfo(
            f'SEG_NODE: label_colors received from {self._label_colors_topic}! '
            'Outputs will have new colors.'
        )


    def publisher(self):
        while not rospy.is_shutdown():
            time.sleep(.05)
            for channel in self._seg_channels:
                channel.seg_and_pub_last_msg()


def main():
    ImgSegNode()
    rospy.spin()


if __name__ == '__main__':
    main()
