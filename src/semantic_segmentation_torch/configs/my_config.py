import semantic_segmentation_torch.package_settings as settings

from .base_config import BaseConfig


class MyConfig(BaseConfig):
    def __init__(self,):
        super(MyConfig, self).__init__()
        settings.initialize('semantic_segmentation_torch')
        # # Dataset
        self.dataset = 'cityscapes'
        # self.data_root = '/path/to/your/dataset'
        self.num_class = 19

        # Model
        self.model = 'liteseg'
        self.logger_name = 'seg_trainer'

        # Validating
        # self.val_bs = 10

        # Testing
        self.is_testing = True
        self.test_bs = 8
        self.test_data_folder = settings.data_path('')
        self.load_ckpt_path = settings.checkpoints_path('liteseg.pth')
        self.save_mask = True

        # Augmentation
        self.crop_size = 768
        self.randscale = [-0.5, 1.0]
        self.scale = 1.0
        self.brightness = 0.5
        self.contrast = 0.5
        self.saturation = 0.5
        self.h_flip = 0.5

        self.save_dir = settings.save_path('')
