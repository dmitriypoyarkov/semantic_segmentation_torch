# Semantic Segmentation Torch

## Установка:
1. Клонировать в Catkin Workspace репозиторий:
```bash
git clone https://gitlab.com/dmitriypoyarkov/semantic_segmentation_torch/
```

2. Выполнить установку catkin пакета:
```bash
cd <catkin_ws>
rosdep install --from-paths src --ignore-src -r -y
catkin build
```

3. Установить pip зависимости:
```bash
cd semantic_segmentation_torch
pip install -r requirements.txt
```

4. Проверить работу на тестовой картинке:
```bash
cd semantic_segmentation_torch
python3 scripts/test.py
```

5. (Опционально) Проверить время, затрачиваемое на сегментацию:
```bash
cd semantic_segmentation_torch
python3 scripts/test_time.py
```
## Узлы
1. `seg_node`  
Узел для запуска сегментации. Имеет настраиваемые входные и выходные топики (любое количество, вплоть до 10).  

    **Подписывается на топики:**
    - `input_img_topic_<num> (sensor_msgs/Image)` - топики для входных изображений.
    - `input_img_topic_<num> (sensor_msgs/Image)` - топики для выходных сегментированных изображений.
    - `input_label_colors (String)` - топик для получения label_colors от системы. Перекрашивает выход сегментации в соответствии с требуемыми цветами. Формат: json-массив с цветами формата `[[r, g, b], ... ]`.  

    **Параметры:**  
    Входные и выходные топики изменяется при помощи параметров. Кроме этого:
    - `run_without_label_colors (bool)` - если true, то сегментация будет производиться даже без label_colors. Сегментированное изображение будет раскрашено в цвета по-умолчанию. После получения label_colors, цвета изменятся на нужные.

## Launch-файлы
1. `seg_ritrover.launch`:  
Ланч для запуска самостоятельного узла сегментации. Содержит топики для робота ritrover.

## Проблемы
1. Если при запуске скриптов пишет "cv2.error: The function is not implemented", нужно переустановить opencv:
    ```
    pip uninstall opencv-python-headless
    pip uninstall opencv-python
    pip install opencv-python
    ```